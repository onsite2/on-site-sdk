package com.sovos.onsite

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.sovos.on_site_mobile.EvidenceStatus
import com.sovos.on_site_mobile.Finger
import com.sovos.on_site_mobile.OnSiteClient
import com.sovos.on_site_mobile.dialog.CaptureEvidenceDialog
import com.sovos.onsite.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        val resultCode = result.resultCode
        val data = result.data

        val description = data?.getStringExtra("DESCRIPTION") ?: "DESCONOCIDO"
        Toast.makeText(this, description, Toast.LENGTH_SHORT).show()
    }

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val client = OnSiteClient.getInstance(this, lifecycle)
        binding.apply {
            evidenceButton.setOnClickListener {
                client.captureEvidence("dni", Finger.RIGHT_INDEX) { status ->
                    when (status) {
                        EvidenceStatus.Connecting -> binding.status.text = "Conectando"
                        is EvidenceStatus.Error -> binding.status.text = status.message
                        EvidenceStatus.GettingAudit -> binding.status.text = "Obteniendo auditoría"
                        is EvidenceStatus.Success -> {
                            binding.status.text = "OK"
                            binding.finger.setImageBitmap(status.image.getBitmap())
                        }

                        EvidenceStatus.WaitingFinger -> binding.status.text = "Coloque huella en el lector"
                    }
                }
            }

            evidenceActivityButton.setOnClickListener {
                val intent = Intent("com.sovos.onsite.EVIDENCIA").apply {
                    putExtra("DNI", "17162369-3")
                    putExtra("FINGER", 5)
                }
                launcher.launch(intent)
            }

            evidenceDialogButton.setOnClickListener {
                CaptureEvidenceDialog
                    .create(this@MainActivity)
                    .setDni("17162369-3")
                    .setFinger(Finger.RIGHT_INDEX)
                    .onSuccess { dni, audit, finger, image, info ->
                        Toast.makeText(this@MainActivity, audit, Toast.LENGTH_SHORT).show()
                    }
                    .onError { code, message, dni, finger ->
                        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                    }
                    .show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            val description = data?.getStringExtra("DESCRIPTION") ?: "DESCONOCIDO"
            Toast.makeText(this, description, Toast.LENGTH_SHORT).show()
        }
    }
}