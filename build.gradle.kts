buildscript {
    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.20")
        classpath("com.android.tools.build:gradle:8.1.2")
        classpath("com.github.dcendents:android-maven-gradle-plugin:2.1")
    }
}
