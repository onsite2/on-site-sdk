package com.sovos.readers_common

import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo


/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 13:18
 */
abstract class FingerprintReader {

    var isOpen = false

    @Throws(Exception::class)
    abstract fun open()

    abstract fun close()

    abstract fun cancelCapture()

    abstract suspend fun capture(): Result<FingerprintImage>

    @Throws(Exception::class)
    abstract suspend fun getInfo(): Result<FingerprintInfo>

    @Throws(Exception::class)
    abstract fun getSerialNumber(): String

    sealed interface Result<out T> {
        class Success<T>(val data: T) : Result<T>
        class Error(val message: String) : Result<Nothing>
    }
}