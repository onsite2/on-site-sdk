package com.sovos.readers_common

import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo


/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 13:18
 */
abstract class FPReader {

    var isOpen = false
    var usePad = false


    @Throws(Exception::class)
    abstract fun open(pad: Boolean = false)

    abstract fun close()

    abstract fun cancelCapture()

    abstract suspend fun capture(): FingerprintImage

    abstract fun qualityToString(quality: Int): String

    @Throws(Exception::class)
    abstract suspend fun getInfo(): FingerprintInfo

    @Throws(Exception::class)
    abstract fun getSerialNumber(): String?
}