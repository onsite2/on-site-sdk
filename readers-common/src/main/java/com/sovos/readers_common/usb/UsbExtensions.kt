package com.sovos.readers_common.usb

import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.util.Log
import com.sovos.readers_common.FingerprintReader
import com.sovos.readers_common.data.DeviceFilter
import java.lang.reflect.InvocationTargetException

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 16:54
 */


enum class MANUFACTURER(val value: String) {
    EIKON("EIKON"), UAREU("UAREU"), SECUGEN("SECUGEN")
}

private val eikonDevice = DeviceFilter(5246, 8214, mManufacturerName = MANUFACTURER.EIKON.name)
private val uareuDevice = DeviceFilter(1466, 10, mManufacturerName = MANUFACTURER.UAREU.name)
private val tocSecuGenDevice = DeviceFilter(4450, 8704, mManufacturerName = MANUFACTURER.SECUGEN.name)

private val suitableFingerprintDeviceList = arrayListOf(
    eikonDevice,
    uareuDevice,
    tocSecuGenDevice
)


fun UsbDevice.deviceId(): Long {
    return (vendorId shl 16 or productId).toLong()
}


fun UsbDevice.isAccessible(context: Context) = context.usbManager.hasPermission(this)


val Context.usbManager: UsbManager
    get() = getSystemService(Context.USB_SERVICE) as UsbManager

val Context.deviceList: HashMap<String, UsbDevice>
    get() = usbManager.deviceList

fun Context.findSuitableFingerprintDevice(): UsbDevice? = deviceList.values.firstOrNull { d ->
    suitableFingerprintDeviceList.any { it.matches(d) }
}

fun UsbDevice.getType(): DeviceFilter {
    return suitableFingerprintDeviceList.first { it.matches(this) }
}

fun DeviceFilter.matches(device: UsbDevice): Boolean {
    if (mVendorId != -1 && device.vendorId != mVendorId)
        return false

    if (mProductId != -1 && device.productId != mProductId)
        return false

    if (matches(device.deviceClass, device.deviceSubclass, device.deviceProtocol)) {
        return true
    }

    val count = device.interfaceCount
    for (i in 0..count) {
        val intf = device.getInterface(i)
        if (matches(intf.interfaceClass, intf.interfaceSubclass, intf.interfaceProtocol))
            return true
    }
    return false
}

private fun DeviceFilter.matches(clazz: Int, subClass: Int, protocol: Int): Boolean =
    (mClass == -1 || clazz == mClass)
            && (mSubclass == -1 || subClass == mSubclass)
            && (mProtocol == -1 || protocol == mProtocol)

fun UsbDevice.getReader(context: Context): FingerprintReader? {
    val deviceFilter = getType()
    return when (deviceFilter.mManufacturerName) {
        MANUFACTURER.EIKON.name -> {
            try {
                return Class.forName("com.sovos.readers_uareu.reader.tc710.TC710Reader", false, context.classLoader).constructors.first()
                    .newInstance(context) as FingerprintReader
            } catch (e: InvocationTargetException) {
                Log.e("INSTANCE_CREATOR", "createInstance: cause: ${e.cause}, target: ${e.targetException}, message: ${e.message}", e)
                return null
            }
            // val instance = Class.forName("com.sovos.readers_uareu.reader.tc710.TC710Reader") as FingerprintReader
//
            // TC710Reader(context)
        }

        MANUFACTURER.UAREU.name -> {
            null
        }

        MANUFACTURER.SECUGEN.name -> {
            null
        }

        else -> null
    }
}