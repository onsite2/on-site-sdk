package com.sovos.readers_common.usb

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 18:03
 */

class UsbChangesBroadcast : BroadcastReceiver() {
    companion object {
        private const val ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED"
        const val DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED"
        const val USB_REQUEST = "onsite.usb.permission.USB_REQUEST"
        const val USB_ACTION = "onsite.usb.permission.USB_ACTION"
    }


    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            ATTACHED -> {
                val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
                device?.let {
                    context.usbManager.requestPermission(
                        device,
                        PendingIntent.getBroadcast(context, 0, Intent(USB_REQUEST), PendingIntent.FLAG_IMMUTABLE)
                    )
                }
            }

            DETACHED -> {
                val send = Intent(USB_REQUEST)
                send.putExtra(USB_ACTION, DETACHED)
                context.sendBroadcast(send)
            }
        }
    }
}