package com.sovos.readers_common.data

import android.hardware.usb.UsbDevice

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 15:43
 */
class DeviceFilter(
    val mVendorId: Int = 0,
    // USB Product ID (or -1 for unspecified)
    val mProductId: Int = 0,
    // USB device or interface class (or -1 for unspecified)
    val mClass: Int = 0,
    // USB device subclass (or -1 for unspecified)
    val mSubclass: Int = 0,
    // USB device protocol (or -1 for unspecified)
    val mProtocol: Int = 0,
    // USB device manufacturer name string (or null for unspecified)
    val mManufacturerName: String? = null,
    // USB device product name string (or null for unspecified)
    val mProductName: String? = null,
    // USB device serial number string (or null for unspecified)
    val mSerialNumber: String? = null,
)
