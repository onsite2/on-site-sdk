package com.sovos.readers_common.data

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 15:05
 */
data class FingerprintInfo(
    val serialNumber: String,
    val version: String,
    val readerType: String
) {
    override fun toString(): String {
        return "SN: $serialNumber" +
                "\nVersion: $version" +
                "\nType: $readerType"
    }
}
