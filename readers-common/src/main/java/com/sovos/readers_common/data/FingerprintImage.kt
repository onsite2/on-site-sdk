package com.sovos.readers_common.data

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import java.nio.ByteBuffer

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 13:20
 */

class FingerprintImage(
    val mWidth: Int,
    val mHeight: Int,
    val mDpi: Int,
    val mRawPixels: ByteArray?,
    var mQuality: Int = -1
) : Parcelable {

    private var mWsqImage: ByteArray? = null
    private val mWsqBitrate = 0.0f
    protected var mBitmap: Bitmap? = null


    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.createByteArray(),
        parcel.readInt(),
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(mWidth)
        parcel.writeInt(mHeight)
        parcel.writeInt(mDpi)
        parcel.writeByteArray(mRawPixels)
        parcel.writeInt(mQuality)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FingerprintImage> {
        override fun createFromParcel(parcel: Parcel): FingerprintImage {
            return FingerprintImage(parcel)
        }

        override fun newArray(size: Int): Array<FingerprintImage?> {
            return arrayOfNulls(size)
        }
    }

    //@Throws(IOException::class)
    //fun getImageWSQ(): ByteArray? {
    //    if (mWsqImage == null) {
    //        mWsqImage = ImageUtils.rawToWSQ(mRawPixels, mWidth, mHeight, mDpi, 8)
    //    }
    //    return mWsqImage
    //}

    fun getRotated180(): FingerprintImage {
        val rotated = ByteArray(mRawPixels!!.size)
        for (i in mRawPixels.indices) {
            rotated[rotated.size - i - 1] = mRawPixels.get(i)
        }
        return FingerprintImage(mWidth, mHeight, mDpi, rotated)
    }

    fun getBitmap(): Bitmap {
        if (mBitmap == null) {
            val rgbaPixels = ByteArray(mWidth * mHeight * 4)
            for (i in 0 until mWidth * mHeight) {
                rgbaPixels[i * 4 + 2] = mRawPixels!![i]
                rgbaPixels[i * 4 + 1] = rgbaPixels[i * 4 + 2]
                rgbaPixels[i * 4] = rgbaPixels[i * 4 + 1]
                rgbaPixels[i * 4 + 3] = -1
            }
            mBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888)
            mBitmap!!.copyPixelsFromBuffer(ByteBuffer.wrap(rgbaPixels))
        }
        return mBitmap!!
    }

    fun scaleToDPI(dpi: Int): FingerprintImage? {
        val scaleFactor: Float = dpi.toFloat() / mDpi
        val tmpBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ALPHA_8)
        tmpBitmap.copyPixelsFromBuffer(ByteBuffer.wrap(mRawPixels!!))
        val scaledResult = Bitmap.createScaledBitmap(tmpBitmap, (mWidth * scaleFactor).toInt(), (mHeight * scaleFactor).toInt(), true)
        return try {
            val pixels = ByteArray(scaledResult.width * scaledResult.height)
            var pixel = 0
            for (y in 0 until scaledResult.height) {
                var x = 0
                while (x < scaledResult.width) {
                    pixels[pixel] = scaledResult.getPixel(x, y).toByte()
                    x++
                    pixel++
                }
            }
            FingerprintImage(scaledResult.width, scaledResult.height, dpi, pixels)
        } finally {
            tmpBitmap.recycle()
            scaledResult.recycle()
        }
    }
}