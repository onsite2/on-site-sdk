package com.sovos.readers_common.data

import android.hardware.usb.UsbDevice
import com.sovos.readers_common.FingerprintReader

/**
 * @author Felipe.Navarro
 * Created 17-10-23 at 16:02
 */

class FingerprintDevice(
    val usbDevice: UsbDevice,
    val reader: FingerprintReader,
    var info: FingerprintInfo
)