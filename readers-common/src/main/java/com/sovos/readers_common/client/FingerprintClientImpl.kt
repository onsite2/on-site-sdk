package com.sovos.readers_common.client

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.sovos.readers_common.usb.UsbChangesBroadcast.Companion.DETACHED
import com.sovos.readers_common.usb.UsbChangesBroadcast.Companion.USB_ACTION
import com.sovos.readers_common.usb.UsbChangesBroadcast.Companion.USB_REQUEST
import com.sovos.readers_common.FingerprintReader
import com.sovos.readers_common.data.FingerprintDevice
import com.sovos.readers_common.data.FingerprintInfo
import com.sovos.readers_common.usb.findSuitableFingerprintDevice
import com.sovos.readers_common.usb.getReader
import com.sovos.readers_common.usb.isAccessible
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * @author Felipe.Navarro
 * Created 30-10-23 at 12:00
 */
internal class FingerprintClientImpl(
    private val context: Context, private val lifecycle: Lifecycle, override val coroutineContext: CoroutineContext = Dispatchers.Main
) : FingerPrintClient, DefaultLifecycleObserver, CoroutineScope {
    companion object {
        private const val TAG = "FingerprintClient"
    }

    private var isCancelled = false

    private var mDevice: FingerprintDevice? = null

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getStringExtra(USB_ACTION) == DETACHED) {
                mDevice = null
                usbChangesListener?.onDeviceDisconnected()
            }
        }
    }

    init {
        launch {
            Log.d(TAG, "INIT")
            lifecycle.addObserver(this@FingerprintClientImpl)
        }
    }

    override var usbChangesListener: FingerPrintClient.UsbDeviceChangeListener? = null
    override var captureListener: FingerPrintClient.FingerprintCaptureListener? = null

    override suspend fun startConnection2(): FingerPrintClient.ConnectionResult {
        return suspendCoroutine {
            usbChangesListener?.connectingToDevice()
            val usbDevice = context.findSuitableFingerprintDevice()
            if (usbDevice == null) {
                //usbChangesListener?.onDeviceConnectionError("No se encontró dispositivo compatible")
                it.resume(FingerPrintClient.ConnectionResult.Error(1, "No se encontró dispositivo compatible"))
                return@suspendCoroutine
            }
            val isAccessible = usbDevice.isAccessible(context) ?: false
            if (!isAccessible) {
                //usbChangesListener?.onDeviceConnectionError("No se han concedido permisos para el dispositivo")
                it.resume(FingerPrintClient.ConnectionResult.Error(2, "No se han concedido permisos para el dispositivo"))
                return@suspendCoroutine
            }
            val reader = usbDevice.getReader(context)
            if (reader == null) {
                it.resume(FingerPrintClient.ConnectionResult.Error(3, "Dispositivo no soportado"))
                return@suspendCoroutine
                //usbChangesListener?.onDeviceConnectionError("Dispositivo no soportado")
            }
            launch {
                val info = reader.getInfo()
                info.apply {
                    when (this) {
                        is FingerprintReader.Result.Error -> {
                            it.resume(FingerPrintClient.ConnectionResult.Error(4, message))
                            //usbChangesListener?.onDeviceConnectionError(message)
                        }

                        is FingerprintReader.Result.Success -> {
                            mDevice = FingerprintDevice(usbDevice, reader, data)
                            usbChangesListener?.onDeviceConnected()
                            it.resume(FingerPrintClient.ConnectionResult.Success(data))
                        }
                    }
                }
            }
        }
    }

    override fun startConnection() {

        launch {
            usbChangesListener?.connectingToDevice()
            val usbDevice = context.findSuitableFingerprintDevice()
            if (usbDevice == null) {
                usbChangesListener?.onDeviceConnectionError("No se encontró dispositivo compatible")
                return@launch
            }
            val isAccessible = usbDevice.isAccessible(context) ?: false
            if (!isAccessible) {
                usbChangesListener?.onDeviceConnectionError("No se han concedido permisos para el dispositivo")
                return@launch
            }
            val reader = usbDevice.getReader(context)
            if (reader == null) {
                usbChangesListener?.onDeviceConnectionError("Dispositivo no soportado")
                return@launch
            }
            val info = reader.getInfo()
            info.apply {
                when (this) {
                    is FingerprintReader.Result.Error -> {
                        usbChangesListener?.onDeviceConnectionError(message)
                    }

                    is FingerprintReader.Result.Success -> {
                        mDevice = FingerprintDevice(usbDevice, reader, data)
                        usbChangesListener?.onDeviceConnected()
                    }
                }
            }

        }
    }

    override suspend fun startCapture2(): FingerPrintClient.CaptureResult {
        return suspendCoroutine {
            if (mDevice == null) {
                //usbChangesListener?.onDeviceConnectionError("Dispositivo no conectado")
                it.resume(FingerPrintClient.CaptureResult.Error(1, "Dispositivo no conectado"))
                return@suspendCoroutine
            }
            if (!mDevice!!.usbDevice.isAccessible(context)) {
                //usbChangesListener?.onDeviceConnectionError("Dispositivo sin permisos")
                it.resume(FingerPrintClient.CaptureResult.Error(1, "Dispositivo sin permisos"))
                return@suspendCoroutine
            }
            launch {
                mDevice!!.reader.capture().apply {
                    when (this) {
                        is FingerprintReader.Result.Error -> {
                            if (isCancelled) {
                                isCancelled = false
                                //captureListener?.onImageCaptureCanceled()
                                it.resume(FingerPrintClient.CaptureResult.Error(200, "Cancelado"))
                            } else {
                                it.resume(FingerPrintClient.CaptureResult.Error(400, message))
                                //captureListener?.onImageCaptureFails(message)
                            }
                        }

                        is FingerprintReader.Result.Success -> {
                            it.resume(FingerPrintClient.CaptureResult.Success(data))
                            //captureListener?.onImageCapture(data.getBitmap())
                        }
                    }
                }
            }
        }
    }

    override fun startCapture() {
        launch {
            if (mDevice == null) {
                usbChangesListener?.onDeviceConnectionError("Dispositivo no conectado")
                return@launch
            }
            if (!mDevice!!.usbDevice.isAccessible(context)) {
                usbChangesListener?.onDeviceConnectionError("Dispositivo sin permisos")
                return@launch
            }
            captureListener?.onCaptureStart()
            mDevice!!.reader.capture().apply {
                when (this) {
                    is FingerprintReader.Result.Error -> {
                        if (isCancelled) {
                            isCancelled = false
                            captureListener?.onImageCaptureCanceled()
                        } else {
                            captureListener?.onImageCaptureFails(message)
                        }
                    }

                    is FingerprintReader.Result.Success -> {
                        captureListener?.onImageCapture(data.getBitmap())
                    }
                }
            }
        }
    }

    override fun cancelCapture() {
        isCancelled = true
        mDevice?.reader?.cancelCapture()
    }

    override fun getDeviceInfo(): FingerprintInfo {
        return mDevice!!.info
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.registerReceiver(receiver, IntentFilter(USB_REQUEST), Context.RECEIVER_NOT_EXPORTED)
        } else {
            context.registerReceiver(receiver, IntentFilter(USB_REQUEST))
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        launch {
            try {
                context.unregisterReceiver(receiver)
            } catch (e: Exception) {
                Log.d(TAG, "Error when trying unregister receiver", e)
            } finally {
                mDevice?.reader?.close()
                mDevice = null
            }

            owner.lifecycle.removeObserver(this@FingerprintClientImpl)
        }
    }

}