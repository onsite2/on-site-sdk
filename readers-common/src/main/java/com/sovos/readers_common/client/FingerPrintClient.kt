package com.sovos.readers_common.client

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo
import kotlinx.coroutines.Dispatchers
import java.lang.reflect.InvocationTargetException

/**
 * @author Felipe.Navarro
 * Created 30-10-23 at 12:00
 */
interface FingerPrintClient {
    var usbChangesListener: UsbDeviceChangeListener?
    var captureListener: FingerprintCaptureListener?

    interface UsbDeviceChangeListener {
        fun connectingToDevice()
        fun onDeviceConnected()
        fun onDeviceConnectionError(message: String)
        fun onDeviceDisconnected()
    }

    interface FingerprintCaptureListener {
        fun onCaptureStart()
        fun onImageCapture(image: Bitmap)
        fun onImageCaptureFails(msg: String = "")
        fun onImageCaptureCanceled()
    }

    sealed interface CaptureResult {
        data class Error(val code: Int, val message: String) : CaptureResult
        data class Success(val image: FingerprintImage) : CaptureResult
    }

    sealed interface ConnectionResult {
        data class Error(val code: Int, val message: String) : ConnectionResult
        data class Success(val data: FingerprintInfo) : ConnectionResult
    }

    companion object {
        fun getInstance(context: Context, lifecycle: Any): FingerPrintClient {
            try {
                return Class.forName("com.sovos.readers_common.client.FingerprintClientImpl", false, context.classLoader).constructors.first()
                    .newInstance(context, lifecycle, Dispatchers.Main) as FingerPrintClient
            } catch (e: InvocationTargetException) {
                Log.e("INSTANCE_CREATOR", "createInstance: cause: ${e.cause}, target: ${e.targetException}, message: ${e.message}", e)
                throw Exception("Error de integración")
            }
        }
    }

    fun startConnection() {}
    suspend fun startConnection2(): ConnectionResult {
        return ConnectionResult.Error(500, "Not implemented")
    }

    fun startCapture() {}
    suspend fun startCapture2(): CaptureResult = CaptureResult.Error(500, "Not implemented")
    fun cancelCapture() {}
    fun getDeviceInfo(): FingerprintInfo? {
        return null
    }
}