package com.sovos.on_site_mobile

/**
 * @author Felipe.Navarro
 * Created 31-10-23 at 11:54
 */
enum class Finger(val value: Int) {
    /**
     * Meñique Izquierdo
     */
    LEFT_PINKY(0) {
        override fun toString(): String {
            return "Meñique Izquierdo"
        }
    },

    /**
     * Anular Izquierdo
     */
    LEFT_RING(1) {
        override fun toString(): String {
            return "Anular Izquierdo"
        }
    },

    /**
     * Corazón Izquierdo
     */
    LEFT_MIDDLE(2) {
        override fun toString(): String {
            return "Corazón Izquierdo"
        }
    },

    /**
     * Índice Izquierdo
     */
    LEFT_INDEX(3) {
        override fun toString(): String {
            return "Índice Izquierdo"
        }
    },

    /**
     * Pulgar Izquierdo
     */
    LEFT_THUMB(4) {
        override fun toString(): String {
            return "Pulgar Izquierdo"
        }
    },

    /**
     * Pulgar Derecho
     */
    RIGHT_THUMB(5) {
        override fun toString(): String {
            return "Pulgar Derecho"
        }
    },

    /**
     * Índice Derecho
     */
    RIGHT_INDEX(6) {
        override fun toString(): String {
            return "Índice Derecho"
        }
    },

    /**
     * Corazón Derecho
     */
    RIGHT_MIDDLE(7) {
        override fun toString(): String {
            return "Corazón Derecho"
        }
    },

    /**
     * Anular Derecho
     */
    RIGHT_RING(8) {
        override fun toString(): String {
            return "Anular Derecho"
        }
    },

    /**
     * Meñique Derecho
     */
    RIGHT_PINKY(9) {
        override fun toString(): String {
            return "Meñique Derecho"
        }
    }
}