package com.sovos.on_site_mobile.activity

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.sovos.on_site_mobile.Finger
import com.sovos.on_site_mobile.dialog.CaptureEvidenceDialog
import com.sovos.on_site_mobile.extras.EvidenceCaptureExtras
import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo


internal class CaptureEvidenceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )

        val extras = intent.extras
        when {
            extras == null -> setError(400, "FALTA INFORMACIÓN OBLIGATORIA", null, null)
            !extras.containsKey(EvidenceCaptureExtras.In.FINGER) -> setError(403, "DEBE INDICAR LA HUELLA", null, null)
            !extras.containsKey(EvidenceCaptureExtras.In.DNI) -> setError(403, "DEBE INGRESAR EL RUT", null, null)
            else -> {
                val dni = extras.getString(EvidenceCaptureExtras.In.DNI)
                if (dni.isNullOrEmpty()) {
                    setError(403, "DEBE INGRESAR EL RUT", null, null)
                    return
                }
                val inputFinger = extras.getInt(EvidenceCaptureExtras.In.FINGER, -1)
                if (inputFinger < 0 || inputFinger > 9) {
                    setError(403, "HUELLA SELECCIONADA NO EXISTE", dni, null)
                    return
                }
                val finger = Finger.values()[inputFinger]

                CaptureEvidenceDialog
                    .create(this)
                    .setDni(dni)
                    .setFinger(finger)
                    .onSuccess { dni, audit, finger, image, info ->
                        setSuccess(dni, finger, audit, image, info)
                    }
                    .onError { code, message, dni, finger ->
                        setError(code, message, dni, finger)
                    }
                    .show()
            }
        }

    }

    private fun setSuccess(dni: String, finger: Finger, audit: String, image: FingerprintImage, info: FingerprintInfo) {
        val result = Intent()
        result.putExtra(EvidenceCaptureExtras.Out.CODE, 200)
        result.putExtra(EvidenceCaptureExtras.Out.DESCRIPTION, "OK")
        result.putExtra(EvidenceCaptureExtras.Out.AUDIT, audit)
        result.putExtra(EvidenceCaptureExtras.Out.DNI, dni)
        result.putExtra(EvidenceCaptureExtras.Out.IMAGE, image.getBitmap())
        result.putExtra(EvidenceCaptureExtras.Out.FINGER, finger.value)
        result.putExtra(EvidenceCaptureExtras.Out.READER, info.serialNumber)
        setResult(200, result)
        finish()
    }

    private fun setError(code: Int, message: String, dni: String?, finger: Finger?) {
        val result = Intent()
        result.putExtra(EvidenceCaptureExtras.Out.CODE, code)
        result.putExtra(EvidenceCaptureExtras.Out.DESCRIPTION, message)
        dni?.let {
            result.putExtra(EvidenceCaptureExtras.Out.DNI, it)
        }
        finger?.let {
            result.putExtra(EvidenceCaptureExtras.Out.FINGER, it.value)
        }
        setResult(code, result)
        finish()
    }
}