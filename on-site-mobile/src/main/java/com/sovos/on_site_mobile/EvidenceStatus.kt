package com.sovos.on_site_mobile

import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo

/**
 * @author Felipe.Navarro
 * Created 06-11-23 at 16:51
 */
sealed interface EvidenceStatus {
    object Connecting : EvidenceStatus
    object WaitingFinger : EvidenceStatus
    object GettingAudit : EvidenceStatus
    data class Error(val code: Int, val message: String) : EvidenceStatus
    data class Success(val audit: String, val image: FingerprintImage, val info: FingerprintInfo) : EvidenceStatus
}