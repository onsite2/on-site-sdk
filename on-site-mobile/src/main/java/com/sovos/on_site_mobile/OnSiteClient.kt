package com.sovos.on_site_mobile

import android.content.Context
import android.util.Log
import androidx.lifecycle.Lifecycle
import kotlinx.coroutines.Dispatchers
import java.lang.reflect.InvocationTargetException

/**
 * @author Felipe.Navarro
 * Created 06-11-23 at 16:51
 */
interface OnSiteClient {
    companion object {
        fun getInstance(context: Context, lifecycle: Lifecycle): OnSiteClient {
            try {
                return Class.forName("com.sovos.on_site_mobile.OnSiteClientImpl", false, context.classLoader).constructors.first()
                    .newInstance(context, lifecycle, Dispatchers.Main) as OnSiteClient
            } catch (e: InvocationTargetException) {
                Log.e("INSTANCE_CREATOR", "createInstance: cause: ${e.cause}, target: ${e.targetException}, message: ${e.message}", e)
                throw Exception("Error de integración")
            }
        }
    }

    fun captureEvidence(dni: String, finger: Finger, onStatusChange: (EvidenceStatus) -> Unit = {})
}

