package com.sovos.on_site_mobile.dialog

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.sovos.on_site_mobile.Finger
import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo
import java.lang.reflect.InvocationTargetException

/**
 * @author Felipe.Navarro
 * Created 07-11-23 at 16:16
 */
interface CaptureEvidenceDialog {
    companion object {
        private const val CLASS_NAME = "com.sovos.on_site_mobile.dialog.CaptureEvidenceDialogImpl"
        fun create(context: AppCompatActivity): CaptureEvidenceDialog {
            try {
                return Class.forName(CLASS_NAME, false, context.classLoader).constructors.first()
                    .newInstance(context) as CaptureEvidenceDialog
            } catch (e: InvocationTargetException) {
                Log.e("INSTANCE_CREATOR", "createInstance: cause: ${e.cause}, target: ${e.targetException}, message: ${e.message}", e)
                throw Exception("Error de integración")
            }

        }
    }

    fun onSuccess(onConfirm: (dni: String, audit: String, finger: Finger, image: FingerprintImage, info: FingerprintInfo) -> Unit): CaptureEvidenceDialog

    fun onError(onError: (code: Int, message: String, dni: String, finger: Finger) -> Unit): CaptureEvidenceDialog

    fun show(): CaptureEvidenceDialog

    fun setDni(dni: String): CaptureEvidenceDialog

    fun setFinger(finger: Finger): CaptureEvidenceDialog
}