package com.sovos.on_site_mobile.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.sovos.on_site_mobile.EvidenceStatus
import com.sovos.on_site_mobile.Finger
import com.sovos.on_site_mobile.OnSiteClient
import com.sovos.on_site_mobile.databinding.DialogBottomCaptureEvidenceBinding
import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @author Felipe.Navarro
 * Created 06-11-23 at 17:42
 */
internal class CaptureEvidenceDialogImpl(private val context: AppCompatActivity) : BottomSheetDialogFragment(), CaptureEvidenceDialog {
    private var _binding: DialogBottomCaptureEvidenceBinding? = null
    private val binding: DialogBottomCaptureEvidenceBinding
        get() = _binding!!

    private var onCaptureSuccess: ((String, String, Finger, FingerprintImage, FingerprintInfo) -> Unit)? = null
    private var onCaptureError: ((code: Int, message: String, dni: String, finger: Finger) -> Unit)? = null
    private var dni: String = ""
    private var finger: Finger = Finger.RIGHT_INDEX

    override fun onSuccess(onConfirm: (dni: String, audit: String, finger: Finger, image: FingerprintImage, info: FingerprintInfo) -> Unit): CaptureEvidenceDialog {
        onCaptureSuccess = onConfirm
        return this
    }

    override fun onError(onError: (code: Int, message: String, dni: String, finger: Finger) -> Unit): CaptureEvidenceDialog {
        onCaptureError = onError
        return this
    }

    override fun show(): CaptureEvidenceDialog {
        show(context.supportFragmentManager, "DIALOG")
        return this
    }

    override fun setDni(dni: String): CaptureEvidenceDialog {
        this.dni = dni
        return this
    }

    override fun setFinger(finger: Finger): CaptureEvidenceDialog {
        this.finger = finger
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogBottomCaptureEvidenceBinding.inflate(inflater, container, false)
        isCancelable = false
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val client = OnSiteClient.getInstance(context, context.lifecycle)
        binding.apply {
            client.captureEvidence(dni, finger) { state ->
                state.apply {
                    when (this) {
                        EvidenceStatus.Connecting -> progressText.text = "Conectando al dispositivo"
                        is EvidenceStatus.Error -> {
                            lifecycleScope.launch {
                                onCaptureError?.invoke(code, message, dni, finger)
                                dismiss()
                            }
                        }

                        EvidenceStatus.GettingAudit -> progressText.text = "Obteniendo auditoría"
                        is EvidenceStatus.Success -> {
                            lifecycleScope.launch {
                                loading.isVisible = false
                                fingerImage.setImageBitmap(image.getBitmap())
                                fingerImage.isVisible = true
                                delay(1000)
                                onCaptureSuccess?.invoke(dni, audit, finger, image, info)
                                dismiss()
                            }
                        }

                        EvidenceStatus.WaitingFinger -> progressText.text = "Coloque su dedo ${finger.toString()}"
                    }
                }
            }


        }
    }
}