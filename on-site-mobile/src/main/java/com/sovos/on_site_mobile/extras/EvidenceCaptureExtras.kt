package com.sovos.on_site_mobile.extras

/**
 * @author Felipe.Navarro
 * Created 06-11-23 at 17:34
 */
interface EvidenceCaptureExtras {
    object In {
        const val DNI: String = "DNI"
        const val FINGER: String = "FINGER"
    }

    object Out {
        const val CODE: String = "CODE"
        const val DESCRIPTION: String = "DESCRIPTION"
        const val AUDIT: String = "AUDIT"
        const val IMAGE: String = "IMAGE"
        const val DNI: String = "DNI"
        const val FINGER: String = "FINGER"
        const val READER: String = "READER"
    }
}