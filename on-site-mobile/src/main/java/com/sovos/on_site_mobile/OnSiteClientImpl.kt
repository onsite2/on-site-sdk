package com.sovos.on_site_mobile

import android.content.Context
import androidx.lifecycle.Lifecycle
import com.sovos.readers_common.client.FingerPrintClient
import com.sovos.readers_common.data.FingerprintInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

/**
 * @author Felipe.Navarro
 * Created 30-10-23 at 14:36
 */

internal class OnSiteClientImpl(
    private val context: Context,
    private val lifecycle: Lifecycle,
    override val coroutineContext: CoroutineContext = Dispatchers.Main
) : OnSiteClient, CoroutineScope {
    private lateinit var fingerPrintClient: FingerPrintClient

    private fun init() {
        fingerPrintClient = FingerPrintClient.getInstance(context, lifecycle)
    }

    override fun captureEvidence(dni: String, finger: Finger, onStatusChange: (EvidenceStatus) -> Unit) {
        launch {
            init()
            onStatusChange(EvidenceStatus.Connecting)
            fingerPrintClient.startConnection2().apply {
                when (this) {
                    is FingerPrintClient.ConnectionResult.Success -> {
                        startCapture(data, dni, finger, onStatusChange)
                    }

                    is FingerPrintClient.ConnectionResult.Error -> {
                        onStatusChange(EvidenceStatus.Error(code, message))
                    }
                }
            }
        }
    }

    private fun startCapture(info: FingerprintInfo, dni: String, finger: Finger, onStatusChange: (EvidenceStatus) -> Unit) {
        launch {
            onStatusChange(EvidenceStatus.WaitingFinger)
            fingerPrintClient.startCapture2().apply {
                when (this) {
                    is FingerPrintClient.CaptureResult.Error -> {
                        onStatusChange(EvidenceStatus.Error(code, message))
                    }

                    is FingerPrintClient.CaptureResult.Success -> {
                        //TODO: Send to backend
                        onStatusChange(EvidenceStatus.GettingAudit)
                        delay(1000)
                        onStatusChange(EvidenceStatus.Success("SUPER AUDITORIA", image, info))
                    }
                }
            }
        }
    }

}