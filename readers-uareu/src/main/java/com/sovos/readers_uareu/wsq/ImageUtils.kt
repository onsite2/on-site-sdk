package com.sovos.readers_uareu.wsq

import com.digitalpersona.uareu.Compression
import com.digitalpersona.uareu.UareUException
import com.digitalpersona.uareu.UareUGlobal
import java.io.IOException

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 13:35
 */
object ImageUtils {
    /**
     * @param rawImage
     * @param width
     * @param height
     * @param dpi
     * @param bpp
     * @return
     * @throws IOException
     * @throws UareUException
     */
    @Throws(IOException::class, UareUException::class)
    fun rawToWSQ(
        rawImage: ByteArray?, width: Int, height: Int,
        dpi: Int, bpp: Int
    ): ByteArray? {
        val compression: Compression = UareUGlobal.GetCompression()
        val wsq = try {
            compression.Start()
            compression.SetWsqBitrate(200, 1)
            compression.CompressRaw(rawImage, width, height, dpi, bpp, Compression.CompressionAlgorithm.COMPRESSION_WSQ_NIST)
        } catch (e: Throwable) {
            throw e
        } finally {
            compression.Finish()
        }
        return wsq
    }
}