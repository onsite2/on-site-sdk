package com.sovos.readers_uareu

import com.digitalpersona.uareu.Reader
import com.digitalpersona.uareu.UareUException
import com.digitalpersona.uareu.UareUGlobal

/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 12:21
 */

object Globals {
    val DefaultImageProcessing: Reader.ImageProcessing = Reader.ImageProcessing.IMG_PROC_DEFAULT

    @Throws(UareUException::class)
    fun getReader(context: Any): Reader? {
        val collection = UareUGlobal.GetReaderCollection(context)
        collection?.GetReaders()
        return collection?.firstOrNull()
    }

    fun getFirstDPI(reader: Reader): Int {
        val caps = reader.GetCapabilities()
        return caps.resolutions[0]
    }
}