package com.sovos.readers_uareu.reader.tc710

import android.util.Log
import com.digitalpersona.uareu.Fid
import com.digitalpersona.uareu.Reader
import com.digitalpersona.uareu.UareUException
import com.sovos.readers_common.FingerprintReader
import com.sovos.readers_common.data.FingerprintImage
import com.sovos.readers_common.data.FingerprintInfo
import com.sovos.readers_uareu.Globals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.min


/**
 * @author Felipe.Navarro
 * Created 12-10-23 at 15:22
 */
class TC710Reader(context: Any) : FingerprintReader() {
    private var mReader: Reader? = null
    private val TAG = this.javaClass.simpleName
    private var mDpi = 500
    private var padEnable = false

    init {
        try {
            mReader = Globals.getReader(context)
        } catch (e: UareUException) {
            Log.w(this.javaClass.name, e.message!!)
        }
    }

    @Throws(Exception::class)
    override fun open() {
        try {
            mReader?.apply {
                Open(Reader.Priority.EXCLUSIVE)
                mDpi = Globals.getFirstDPI(this)
                setPadEnable(1)
                isOpen = true
            }
        } catch (e: Exception) {
            Log.e(TAG, "OPEN ERROR", e)
            throw e
        }
    }

    private fun setPadEnable(enable: Int) {
        val isEnable = enable == 1
        try {
            mReader?.SetParameter(Reader.ParamId.DPFPDD_PARMID_PAD_ENABLE, byteArrayOf(enable.toByte()))
            padEnable = isEnable
        } catch (e: UareUException) {
            Log.e(TAG, "PAD cannot be enabled", e)
        }
    }

    override fun close() {
        try {
            if (padEnable) {
                setPadEnable(0)
            }
            mReader?.Close()
            isOpen = false
        } catch (e: Exception) {
            Log.w(TAG, "Close error", e)
        }
    }

    override fun cancelCapture() {
        try {
            mReader?.CancelCapture()
        } catch (e: java.lang.Exception) {
            Log.w(TAG, "Cancel capture Error", e)
        } finally {
            close()
        }
    }

    override suspend fun capture(): Result<FingerprintImage> {
        return withContext(Dispatchers.IO) {
            try {
                if (!isOpen) open()
                if (mReader == null) {
                    Result.Error("Error de comunicación con el huellero")
                } else {
                    //This get result when user interacts with the hw
                    //Capture(Format, ImageProcessing, DPI, Timeout)
                    val capture = mReader!!.Capture(Fid.Format.ANSI_381_2004, Globals.DefaultImageProcessing, mDpi, -1)
                    val image = capture.image
                    if (image != null) {
                        val fiv = image.views[0]
                        val fingerImage = fiv.imageData
                        val fi = FingerprintImage(
                            mWidth = fiv.width,
                            mHeight = fiv.height,
                            mDpi = image.imageResolution,
                            mRawPixels = fingerImage,
                            mQuality = capture.quality.ordinal
                        )
                        Result.Success(fi)
                    } else {
                        Result.Error("Error al capturar la huella")
                    }
                }
            } catch (e: Exception) {
                Result.Error(e.message ?: "Error desconocido")
            } finally {
                close()
            }
        }
    }

    override suspend fun getInfo(): Result<FingerprintInfo> {
        return try {
            if (!isOpen) open()

            val description = mReader!!.GetDescription()
            val serialNumber = getSerialNumber()
            val version = String.format(
                "%s.%s.%s",
                description.version.firmware_version.major,
                description.version.firmware_version.minor,
                description.version.firmware_version.maintenance
            )
            return Result.Success(
                FingerprintInfo(
                    serialNumber = serialNumber,
                    version = version,
                    readerType = "reader.eikon"
                )
            )
        } catch (e: Exception) {
            Result.Error("${e.message}")
        } finally {
            close()
        }
    }

    @Throws(Exception::class)
    override fun getSerialNumber(): String {
        var strPtapiGuid = "N/A"
        try {
            val guid = mReader!!.GetParameter(Reader.ParamId.PARMID_PTAPI_GET_GUID)
            if (16 == guid.size) {
                val hexArray = "0123456789ABCDEF".toCharArray()
                val hexChars = CharArray(guid.size * 2)
                for (j in guid.indices) {
                    val v = guid[j].toInt() and 0xFF
                    hexChars[j * 2] = hexArray[v ushr 4]
                    hexChars[j * 2 + 1] = hexArray[v and 0x0F]
                }
                strPtapiGuid = String(hexChars)
            }
            val original: List<String> = getParts(strPtapiGuid, 2)
            val str = StringBuilder()
            str.append("{")
            str.append(original[3])
            str.append(original[2])
            str.append(original[1])
            str.append(original[0])
            str.append("-")
            str.append(original[5])
            str.append(original[4])
            str.append("-")
            str.append(original[7])
            str.append(original[6])
            str.append("-")
            str.append(original[8])
            str.append(original[9])
            str.append("-")
            str.append(original[10])
            str.append(original[11])
            str.append(original[12])
            str.append(original[13])
            str.append(original[14])
            str.append(original[15])
            str.append("}")
            return str.toString()
        } catch (e: java.lang.Exception) {
            throw java.lang.Exception("Número de serie inválido")
        }
    }

    private fun getParts(string: String, partitionSize: Int): List<String> {
        val parts: MutableList<String> = ArrayList()
        val len = string.length
        var i = 0
        while (i < len) {
            parts.add(string.substring(i, min(len, i + partitionSize)))
            i += partitionSize
        }
        return parts
    }
}