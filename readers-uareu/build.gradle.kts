plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("maven-publish")
}

android {
    namespace = "com.sovos.readers_uareu"
    compileSdk = 34
    defaultConfig{
        minSdk = 24
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes{
        release {

        }
    }

    publishing {
        singleVariant("release") {
            withSourcesJar()
            withJavadocJar()
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    api(files("libs/dpuareu.jar"))
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    implementation(project(":readers-common"))
}

publishing {
    publications {
        register<MavenPublication>("release") {
            groupId = "com.gitlab.onsite2"
            artifactId = "readers-uareu"
            version = "0.0.1-beta01"

            afterEvaluate {
                from(components["release"])
            }
        }
    }
}